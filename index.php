<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 02</title>
</head>
<body>
	<h2>Activity: Divisibles of Five</h2>

	<p><?php echo divisibleByFive(); ?></p>

	<h2>Array Manipulation</h2>

	<p><?php array_push($students, 'Cyrille Moza') ?></p>
	<pre><?php print_r($students) ?></pre>

	<pre><?php echo count($students) ?></pre>

	<p><?php array_push($students, 'Catherine Marie') ?></p>
	<pre><?php print_r($students) ?></pre>

	<p><?php array_shift($students) ?></p>
	<pre><?php print_r($students) ?></pre>


</body>
</html>